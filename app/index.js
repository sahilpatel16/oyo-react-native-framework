import React from 'react';
import { StatusBar, View, Platform } from 'react-native';

import AppStack from './common/navigations';
import { statusBarColor } from './assets/colors';

const TheApp = () => (
  <View style={{ flex: 1 }}>
    {Platform.OS === 'android' ? (
      <StatusBar backgroundColor={statusBarColor} barStyle="light-content" />
    ) : null}
    <AppStack />
  </View>
);

export default TheApp;

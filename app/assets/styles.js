import { StyleSheet } from 'react-native';
import {
  menuTintColor,
  menuBarBackgroundColor,
  defaultBackgroundColor,
} from './colors';

export const headerTitleStyle = () => ({
  fontSize: 16,
  fontFamily: 'Roboto-Bold',
  fontWeight: '500',
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: defaultBackgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
  },

  menuBarStyle: {
    backgroundColor: menuBarBackgroundColor,
  },
});

export const menuBarNavigationOptions = title => ({
  title,
  headerStyle: styles.menuBarStyle,
  headerTintColor: menuTintColor,
  headerTitleStyle: headerTitleStyle(),
});

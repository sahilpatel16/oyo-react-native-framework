export const menuTintColor = '#FFFFFF';
export const menuBarBackgroundColor = '#3F51B5';
export const statusBarColor = '#303F9F';
export const defaultBackgroundColor = '#EEEEEE';
export const accentColor = '#4CAF50';

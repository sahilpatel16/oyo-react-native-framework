import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, View } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import { accentColor } from '../../assets/colors';

const styles = StyleSheet.create({
  circleButton: {
    width: 60,
    height: 60,
    borderRadius: 100 / 2,
    backgroundColor: accentColor,
    borderColor: 'transparent',
    borderWidth: 0,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 2,
    shadowOpacity: 0.5,
  },
});

const FABIcon = (props) => {
  const { mode } = props;
  return mode === 'edit' ? (
    <MIcon name="edit" size={22} color="white" />
  ) : (
    <Icon type="ionicons" name="md-add" size={22} color="white" />
  );
};

const FAB = (props) => {
  const { onFabPressed, style, mode } = props;

  return (
    <TouchableWithoutFeedback onPress={() => onFabPressed()}>
      <View style={[styles.circleButton, style]}>
        <FABIcon mode={mode} />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default FAB;

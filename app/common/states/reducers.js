/**
 * contains reducers which will be shared throughout the
 * app.
 */

const initialState = {};
const shared = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default shared;

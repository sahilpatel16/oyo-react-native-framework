import { createStackNavigator } from 'react-navigation';

import Splash from '../modules/splash';
import { NAV } from './constants';
import Login from '../modules/login';
import { menuBarNavigationOptions } from '../assets/styles';

const AppStack = createStackNavigator(
  {
    splash: {
      screen: Splash,
      navigationOptions: {
        header: null,
      },
    },
    login: {
      screen: Login,
      navigationOptions: () => menuBarNavigationOptions(''),
    },
  },
  {
    initialRouteName: NAV.SPLASH,
  },
);

export default AppStack;

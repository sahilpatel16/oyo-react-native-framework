export const INTERNAL_ERROR = 666;
export const NETWORK_ERROR = 111;
export const DEFAULT_VALUE = '';
export const TRUE = 'true';
export const FALSE = 'false';

export const NAV = {
  LOGIN: 'login',
  SPLASH: 'splash',
};

export const ERROR_ID = {
  INTERNAL_ERROR: 666,
  NETWORK_ERROR: 111,
  INVALID_URL: 112,
};

export const ERROR_TITLE = {
  INTERNAL_ERROR: 'Internal Error',
  NETWORK_ERROR: 'Network Error',
};

export const ICON_TYPE = {
  MATERIAL_COMMUNITY: 'materialCommunityIcon',
  IONICON: 'ionicon',
  ENTYPO: 'entypo',
  MATERIAL_ICON: 'materialIcon',
  OCTICON: 'octicon',
};

import { INTERNAL_ERROR, ERROR_TITLE, ERROR_ID } from './constants';

export const convertToPreferenceObject = (key, value) => ({
  key,
  value,
});

export const convertToErrorStateObject = (errorObject) => {
  const error = JSON.parse(errorObject.message);

  return {
    code: error.errorCode,
    type: error.errorType,
    message: error.message,
  };
};

export const createInternalErrorObject = errorMessage => new Error(
  JSON.stringify({
    status: INTERNAL_ERROR,
    errorCode: ERROR_ID.INTERNAL_ERROR,
    errorType: ERROR_TITLE.INTERNAL_ERROR,
    message: errorMessage,
  }),
);

export const nameTypePair = (name, type) => ({
  iconName: name,
  iconType: type,
});

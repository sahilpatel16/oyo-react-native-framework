import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NAV } from '../../common/constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class Splash extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      const { navigation } = this.props;
      navigation.navigate(NAV.LOGIN);
    }, 8000);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>
Oyo react native framework
        </Text>
      </View>
    );
  }
}

export default Splash;

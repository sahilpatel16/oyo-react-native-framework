import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  text: {
    marginTop: 20,
    fontSize: 20,
    color: 'grey',
    fontFamily: 'Roboto-Regular',
  },
});

class Login extends React.Component {
  componentDidMount() {}

  render() {
    return (
      <View style={styles.container}>
        <Icon name="thumbs-up" size={100} color="grey" />

        <Text style={styles.text}>
Make something awesome
        </Text>
      </View>
    );
  }
}
export default Login;

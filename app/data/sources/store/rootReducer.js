import { combineReducers } from 'redux';

//  The shared reducer for application state
import shared from '../../../common/states/reducers';

// import other reducers from modules

export default combineReducers({
  shared,
});

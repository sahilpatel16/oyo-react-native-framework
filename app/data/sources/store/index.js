import { createStore, applyMiddleware } from 'redux';

//  used for making asynchronous calls that would
//  emit some redux action when the call completes
import thunk from 'redux-thunk';

//  all the reducers in the app.
import { createLogger } from 'redux-logger';
import rootReducer from './rootReducer';

//  used for creating a logger that logs redux states
//  and actions.

//  creating the logger middleware
const loggerMiddleware = createLogger();

//  creating a new store with our root reducer and
//  middlewares.
const configureStore = preloadedState => createStore(
  rootReducer,
  preloadedState,
  applyMiddleware(thunk, loggerMiddleware),
);

const store = configureStore();
export default store;

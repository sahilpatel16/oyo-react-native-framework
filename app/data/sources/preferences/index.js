import { AsyncStorage } from 'react-native';

//  Saves a value in local storage with the provided key.
//  returns a promise to confirm that the value is saved.
export const save = async (key, value) => {
  try {
    return await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

//  Returns the value that is saved with the passed key in local
//  preference storage. If no such value exists, returns the default
//  value.
//  Returns response as a Promise object.
export const get = async (key, defaultValue) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
    return defaultValue;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

//  Clears everything that is saved locally in preferences.
//  Useful when we are clearing out a user's login session.
//  returns a promise as response
export const clearAll = async () => {
  try {
    return await AsyncStorage.clear();
  } catch (error) {
    console.error(error);
    throw error;
  }
};

//  There are times when we want to save a bunch of key value
//  pairs and get a single promise as response. Use this function
//  for such situations. It returns a single promise when everything
//  is saved successfully or it throws an error.
export const saveAll = async (pairs = []) => {
  try {
    const items = pairs.map(pair => AsyncStorage.setItem(pair.key, pair.value));

    await Promise.all(items);
  } catch (error) {
    console.log(error);
  }
};

export const getAll = async (pairs = []) => {
  try {
    const items = await pairs.map((pair) => {
      const value = AsyncStorage.getItem(pair.key);

      if (value !== null) {
        return value;
      }
      return pair.value;

      //  This code causes the problem
      // const newPair = pair;
      // if (value !== null) {
      //   newPair.value = pair;
      // }
      // return newPair;
    });

    return Promise.all(items);
  } catch (error) {
    console.log(error);
    return Promise.all(pairs);
  }
};

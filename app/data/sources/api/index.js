import axios from 'axios';
import config from './credentials';

//  We will use this error code in case we don't
//  receive an error from the server
import {
  INTERNAL_ERROR,
  NETWORK_ERROR,
  ERROR_TITLE,
  ERROR_ID,
} from '../../../common/constants';

//  adding a default base url
axios.defaults.baseURL = config.base_url;

//  adding a default header for every request.
axios.defaults.headers.common = {
  accept: 'application/json',
  'app-key': config.app_key,
  'app-secret': config.app_secret,
  'x-oyo-lang': 'en',
};

//  adding request and response interceptors. We use these
//  primarily for logging requests and responses
axios.interceptors.request.use((request) => {
  console.log('Starting Request : ', request);
  return request;
});

axios.interceptors.response.use((response) => {
  console.log('Response : ', response);
  return response;
});

const readXMLHttpRequestTypeError = (error) => {
  // eslint-disable-next-line no-underscore-dangle
  const errorResponse = JSON.stringify(error.request._response);
  console.log('Apple');
  console.log(errorResponse);
  if (errorResponse.includes('Unable to resolve host')) {
    return new Error(
      JSON.stringify({
        status: INTERNAL_ERROR,
        errorCode: ERROR_ID.INVALID_URL,
        errorType: ERROR_TITLE.NETWORK_ERROR,
        message: 'Could not connect to the service',
      }),
    );
  }
  return new Error(
    JSON.stringify({
      status: INTERNAL_ERROR,
      errorCode: INTERNAL_ERROR,
      errorType: 'NETWORK ERROR',
      message: error.request,
    }),
  );
};

//  Here we handle network error. The implementation of this
//  function depends upon the rest api that we are connecting to.
//  Here is how it is implemented for rocket app. It might be
//  different for other apps.
const handleNetworkError = (error) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(error.response.data);

    if (!error.response.data.error.code) {
      throw new Error(
        JSON.stringify({
          status: error.response.status,
          errorCode: 401,
          errorType: 'NOT FOUND',
          message: error.response.data.error,
        }),
      );
    } else {
      throw new Error(
        JSON.stringify({
          status: error.response.status,
          errorCode: error.response.data.error.code,
          errorType: error.response.data.error.type,
          message: error.response.data.error.message,
        }),
      );
    }
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    throw readXMLHttpRequestTypeError(error);
  } else {
    // Something happened in setting up the request that triggered an Error
    throw new Error(
      JSON.stringify({
        status: NETWORK_ERROR,
        errorCode: INTERNAL_ERROR,
        errorType: 'NETWORK ERROR',
        message: error.message,
      }),
    );
  }
};

//  Use this method to make any post request
export const postRequest = (url, data) => axios.post(url, data).catch(error => handleNetworkError(error));

//  use this method to make a get request
export const getRequest = (url, data) => axios
  .get(url, {
    params: data,
  })
  .catch(error => handleNetworkError(error));

export const getRequestWithBaseUrl = (url, data, baseURL) => axios({
  method: 'post',
  url,
  baseURL,
  data,
}).catch(error => handleNetworkError(error));

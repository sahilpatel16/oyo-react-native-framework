import React from 'react';
import { Provider } from 'react-redux';
import TheApp from './app/index';

import store from './app/data/sources/store';

const App = () => (
  <Provider store={store}>
    <TheApp />
  </Provider>
);

export default App;

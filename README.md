This is a generic react-native implementation of oyo-react-native-template i.e. the base project that gets created with the command `react-native init [project name]`
The directory structure for javascript files is similar to our [base template](https://bitbucket.org/sahilpatel16/oyo-react-native-framework/src/base/).


## Setup

* Clone the project and checkout to react-init-min branch
* Update the app name and application id to match your particular application. You will have to make this change in android and ios files too.
* We have a few dependencies in the framework. You can install all of it by running `npm install` from the terminal
* Test the app by running `react-native start-android` or `react-native start-ios`


> You can learn more about the directory structure and it's significance from [here](https://bitbucket.org/sahilpatel16/oyo-react-native-framework/src/expo-min/README.md?mode=edit&spa=0&at=expo-min&fileviewer=file-view-default). 